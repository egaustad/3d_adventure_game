﻿using UnityEngine;
using System.Collections;

public class AButton : MonoBehaviour {

	public GameObject adoor;
	public GameObject stationaryblue;

	void Start ()
	{
		stationaryblue.gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Box")) {
			//	AudioClip.(pickUp);
			adoor.gameObject.SetActive (false);
			global.adoor = true;
			other.gameObject.SetActive(false);
			stationaryblue.gameObject.SetActive (true);

		}
	}
}
