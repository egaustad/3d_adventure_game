﻿using UnityEngine;
using System.Collections;

public class lightManager : MonoBehaviour {
	public float m_MaxLifeTime = 2f;                    // The time in seconds before the shell is removed.
	//public int Energy = 100;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, m_MaxLifeTime);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
