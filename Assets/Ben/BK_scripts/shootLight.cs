﻿using UnityEngine;
using UnityEngine.UI;

public class shootLight : MonoBehaviour
{
	public Rigidbody m_LightShot;                   // Prefab of the shell.
	public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
	private float m_CurrentLaunchForce = 20f;         // The force that will be given to the shell when the fire button is released.
	private float m_ChargeSpeed = 20f;                // How fast the launch force increases, based on the max charge time.
	private bool m_Fired;                       // Whether or not the shell has been launched with this button press.

	private void Start (){}

	void OnClick(){
		Fire ();
	}

	private void Update (){

		// ... launch the shell.
		if (Input.GetMouseButton (0) || Input.GetMouseButton (1)) {
			Fire ();
		}
	}


	private void Fire (){
		// Set the fired flag so only Fire is only called once.
		m_Fired = true;

		// Create an instance of the shell and store a reference to it's rigidbody.
		Rigidbody light_Instance =
			Instantiate (m_LightShot, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

		// Set the shell's velocity to the launch force in the fire position's forward direction.
		light_Instance.velocity = m_CurrentLaunchForce * m_FireTransform.forward;
	}
}