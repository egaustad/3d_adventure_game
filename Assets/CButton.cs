﻿using UnityEngine;
using System.Collections;

public class CButton : MonoBehaviour {

	public GameObject cdoor;
	public GameObject stationaryorange;

	void Start ()
	{
		stationaryorange.gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Box")) {
			//	AudioClip.(pickUp);
			cdoor.gameObject.SetActive (false);
			global.cdoor = true;
			other.gameObject.SetActive(false);
			stationaryorange.gameObject.SetActive (true);

		}
	}
}
