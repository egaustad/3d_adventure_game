﻿using UnityEngine;
using System.Collections;

public class DButton : MonoBehaviour {

	public GameObject ddoor;
	public GameObject stationaryred;

	void Start ()
	{
		stationaryred.gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Box")) {
			//	AudioClip.(pickUp);
			ddoor.gameObject.SetActive (false);
			global.ddoor = true;
			other.gameObject.SetActive(false);
			stationaryred.gameObject.SetActive (true);

		}
	}
}
