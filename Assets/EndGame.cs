﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndGame : MonoBehaviour {

	public static bool Won = false;
	public Image redkey;
	public Image bluekey;
	public Image greenkey;
	public Image yellowkey;
	public Image endimage;
	public Text Story;
	public Camera end;
	public GameObject fps;

	void Start ()
	{
		end.gameObject.SetActive (false);
		endimage.gameObject.SetActive (false);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Player")){
			if (pickUp.yellowkeybool && pickUp.bluekeybool && pickUp.greenkeybool && pickUp.redkeybool) {
				Application.LoadLevel (2);
			}
		}
	}
	void Update(){
		if (Won) {
			redkey.gameObject.SetActive (false);
			bluekey.gameObject.SetActive (false);
			greenkey.gameObject.SetActive (false);
			yellowkey.gameObject.SetActive (false);
			Story.gameObject.SetActive (false);

			//player.gameObject.SetActive (false);
			fps.gameObject.SetActive (false);
			end.gameObject.SetActive (true);
			endimage.gameObject.SetActive (true);

			global.endg = true;
		}
	}
}
