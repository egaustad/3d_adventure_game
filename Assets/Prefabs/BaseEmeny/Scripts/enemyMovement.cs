﻿using UnityEngine;
using System.Collections;

public class enemyMovement : MonoBehaviour {

	public Transform player;
	public float thrust = 0.05f;
	public float Timer;
	public float TimerMove;
	public float TimerRun;

	private Vector3 startLoc;
	private Quaternion startRot;

	private bool OutofArea = false;
	private bool Free = false;
	private bool Run = false;

	void Start(){
		startLoc = this.transform.position;
		Vector3 direction = new Vector3(0, Random.Range(0,360), 0);
		this.transform.rotation = Quaternion.Euler (direction);
		Timer = Time.timeSinceLevelLoad + 25.0f;
		TimerMove = Time.timeSinceLevelLoad + 5.0f;
		TimerRun = Time.timeSinceLevelLoad;
	}

	void Update () {
		if (Time.timeScale != 0) {
			this.transform.position += (this.transform.forward * thrust);
			Vector3 direction = player.position - this.transform.position;
			if (Vector3.Distance (player.position, this.transform.position) < 150 && !OutofArea && !Run) {
				Free = false;
				direction.y = 0;

				this.transform.rotation = Quaternion.Lerp (this.transform.rotation, Quaternion.LookRotation (direction), 0.1f);

				if (direction.magnitude > 60) {
					thrust = 0.05f;
				} else if (direction.magnitude > 40) {
					thrust = 0.2f;
				} else if (direction.magnitude > 0) {
					thrust = 0.35f;
				}
			}
			Vector3 direction2 = startLoc - this.transform.position;
			if (Vector3.Distance (startLoc, this.transform.position) > 150 && Run == false) {
				Free = false;
				OutofArea = true;
				direction2.y = 0;
				this.transform.rotation = Quaternion.Euler (direction2);
			}
			if (Vector3.Distance (startLoc, this.transform.position) < 10 || Vector3.Distance (player.position, this.transform.position) < 30 && Run == false) {
				OutofArea = false;
			}
			if (Vector3.Distance (startLoc, this.transform.position) <= 150 && Vector3.Distance (player.position, this.transform.position) > 150 && Run == false) {
				Free = true;
			}
			if (Free && !Run) {
				if (Time.timeSinceLevelLoad > Timer) {
					Timer += 25.0f;
					Vector3 direction3 = new Vector3 (0, Random.Range (0, 360), 0);
					this.transform.rotation = Quaternion.Euler (direction3);
				}
			}
			if (Time.timeSinceLevelLoad > TimerRun && Run) {
				Run = false;
				Vector3 direction3 = new Vector3 (0, Random.Range (0, 360), 0);
				this.transform.rotation = Quaternion.Euler (direction3);
			}
		}
	}
	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "light") {
			Run = true;
			TimerRun = Time.timeSinceLevelLoad + 10;
			this.transform.rotation = player.transform.rotation;
			Destroy (other.gameObject);
			thrust = 0.5f;
		}
	}
}
