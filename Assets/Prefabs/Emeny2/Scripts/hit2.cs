﻿using UnityEngine;
using System.Collections;

public class hit2 : MonoBehaviour {

	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "light") {
			Quaternion angle2 = Quaternion.Euler (0, other.transform.rotation.y, 0);
			this.transform.rotation = angle2;
			Destroy (other.gameObject);
		}
	}
}
