﻿using UnityEngine;
using System.Collections;

public class enemyMovement3 : MonoBehaviour {

	public Transform player3;
	public float thrust3 = 0.3f;
	public float TimerRotate;
	public float TimerRun3;

	private Vector3 startLoc3;
	public Quaternion startRot3;

	private bool Free3 = true;
	private bool Run3 = false;
	private int turnRL = 1;

	void Start(){
		startLoc3 = this.transform.position;
		startRot3 = this.transform.rotation;
		TimerRotate = Time.timeSinceLevelLoad + 15.0f;
		TimerRun3 = Time.timeSinceLevelLoad;
	}

	void Update () {
		if (Time.timeScale != 0) {
			//moves forward
			this.transform.position += (this.transform.forward * thrust3);
			//gets direction
			if (Time.timeSinceLevelLoad >= TimerRotate) {
				TimerRotate = 15.0f + Time.timeSinceLevelLoad;
				if (turnRL == 1) {
					turnRL = 2;
					Vector3 direction8 = new Vector3 (0, (180), 0);
					this.transform.rotation = Quaternion.Euler (direction8) * startRot3;
				} else if (turnRL == 2) {
					turnRL = 1;
					Vector3 direction8 = new Vector3 (0, (0), 0);
					this.transform.rotation = Quaternion.Euler (direction8) * startRot3;
				}
			}
			//if out of the enemy is too far from start location
			Vector3 direction5 = startLoc3 - this.transform.position;
			if (Vector3.Distance (startLoc3, this.transform.position) > 150 && Run3 == false) {
				Free3 = false;
				direction5.y = 0;
				this.transform.rotation = Quaternion.Euler (direction5);
			}
			//sets variable if enemy can move freely
			if (Vector3.Distance (startLoc3, this.transform.position) <= 150 && Run3 == false) {
				Free3 = true;
			}
			//when it is done running from player
			if (Time.timeSinceLevelLoad > TimerRun3 && Run3) {
				Run3 = false;
			}
		}
	}
	void OnTriggerEnter(Collider other){
		if (other.transform.tag == "light") {
			Run3 = true;
			TimerRun3 = Time.timeSinceLevelLoad + 10;
			this.transform.rotation = player3.transform.rotation;
			Destroy (other.gameObject);
			thrust3 = 0.5f;
		}
	}
}
