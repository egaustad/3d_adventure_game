﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class GameDat{
	public Vector3 playerLoc;
	public Quaternion playerRot;
	public bool RedKey;
	public bool GreenKey;
	public bool BlueKey;
	public bool YellowKey;
	public bool HaveWeaponOne;
	public bool HaveWeaponTwo;
	public bool HaveWeaponThree;
	public bool HaveWeaponFour;
	public bool HaveWeaponFive;
	public int LeftHand;
	public int RightHand;
	public float Health;
	public Vector3[] Enemies;
}

public class LoadGame : MonoBehaviour {
	void Start(){
		/*
		if(File.Exists(Application.persistentDataPath+"/hero.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream fs = File.Open(Application.persistentDataPath + "/hero.dat", FileMode.Open);
			HeroDat dat = (HeroDat)bf.Deserialize(fs);
			print("loaded " + dat.height + " " + dat.mysupersecretvaraible);
		}
		else{
			BinaryFormatter bf = new BinaryFormatter();
			HeroDat hd = new HeroDat();
			hd.height = 10;
			hd.mysupersecretvaraible = 7;
			FileStream fs = File.Create(Application.persistentDataPath+"/hero.dat");
			bf.Serialize(fs, hd);
			fs.Close();
			print("saved to file!");
		}
		*/
		if(MenuScript.LoadSave){
			if(File.Exists("OTDsave.dat")){
				BinaryFormatter bf = new BinaryFormatter();
				FileStream fs = File.Open("OTDsave.dat", FileMode.Open);
				//GameDat dat = (global.GameDat)bf.Deserialize(fs);
			}
		}
	}
}