﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;

public class StoryScript : MonoBehaviour {
	public Canvas StoryMenu;
	public static bool StoryMenuVisable;
	public float Timer;

	void Start () {
		Timer = 20;
		StoryMenu = StoryMenu.GetComponent<Canvas> ();
		StoryMenuVisable = true;
	}

	void Update () {
		StoryMenu.enabled = StoryMenuVisable;
		if (Time.timeSinceLevelLoad > Timer){
			PlayGame ();
		}
	}
	public void PlayGame () {
		StoryMenuVisable = false;
	}
}
