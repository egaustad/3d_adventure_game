﻿using UnityEngine;
using System.Collections;

public class detinate : MonoBehaviour {
	public float waitTime = 0f;
	public float thrust;
	public float timeOn = 0.1f;
	public float timeOff = 0.1f;
	public float changeTime = 0f;
	public Light light;

	// Use this for initialization
	void Start () {
		light = GetComponent<Light> ();
		waitTime = Time.deltaTime + 3f;
	}
	
	// Update is called once per frame
	void Update () {
		if (waitTime < Time.deltaTime) {
			
		}
		if (Time.time > changeTime) {
		light.enabled = !light.enabled;
		if (light.enabled) {
			changeTime = Time.time + timeOn;
		} else {
			changeTime = Time.time + timeOff;
		}
	}
}
}
