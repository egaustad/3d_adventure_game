﻿using UnityEngine;
using System.Collections;

public class healthCollider : MonoBehaviour {

	public GameObject healthBar;
	// Use this for initialization
	void Start () {
	
	}

	private void OnTriggerEnter (Collider other)
	{
		if (other.transform.tag == "enemy1") {
			healthBar.GetComponent<HealthBar>().ReduceHealth();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
