# README #

### What is this repository for? ###

This is a Unity folder for a 3D adventure game we created in a Software Engineering class at Point Loma Nazarene University.

I worked with Chris Richey, Jack Higgins, Andrew Taylor, and Ben Khoshaba.

### How do I get set up? ###

Go into Assets -> Scenes -> MainMenu

Open up the MainMenu scene in Unity and you will be able to then run the program properly.


### Who do I talk to? ###

Any questions please contact the admin of this project, Erik Gaustad, at egaustad033@pointloma.edu